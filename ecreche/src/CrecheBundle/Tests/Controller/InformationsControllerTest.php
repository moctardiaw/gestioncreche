<?php

namespace CrecheBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class InformationsControllerTest extends WebTestCase
{
    public function testCreate()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'informations/create');
    }

    public function testRead()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'informations/read');
    }

    public function testUpdate()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'informations/update');
    }

    public function testDelete()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'informations/delete');
    }

}
