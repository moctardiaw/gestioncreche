<?php

namespace CrecheBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ChargesControllerTest extends WebTestCase
{
    public function testCreate()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'charges/create');
    }

    public function testRead()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'charges/read');
    }

    public function testUpdate()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'charges/update');
    }

    public function testDelete()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'charges/delete');
    }

}
