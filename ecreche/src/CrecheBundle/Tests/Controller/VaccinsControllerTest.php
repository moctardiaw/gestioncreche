<?php

namespace CrecheBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class VaccinsControllerTest extends WebTestCase
{
    public function testCreate()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'vaccins/create');
    }

    public function testRead()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'vaccins/read');
    }

    public function testUpdate()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'vaccins/update');
    }

    public function testDelete()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'vaccins/delete');
    }

}
