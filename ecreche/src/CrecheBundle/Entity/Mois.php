<?php

namespace CrecheBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mois
 *
 * @ORM\Table(name="mois")
 * @ORM\Entity(repositoryClass="CrecheBundle\Repository\MoisRepository")
 */
class Mois
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nomMois", type="string", length=255)
     */
    private $nomMois;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomMois
     *
     * @param string $nomMois
     *
     * @return Mois
     */
    public function setNomMois($nomMois)
    {
        $this->nomMois = $nomMois;

        return $this;
    }

    /**
     * Get nomMois
     *
     * @return string
     */
    public function getNomMois()
    {
        return $this->nomMois;
    }
}

