<?php

namespace CrecheBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Charges
 *
 * @ORM\Table(name="charges")
 * @ORM\Entity(repositoryClass="CrecheBundle\Repository\ChargesRepository")
 */
class Charges
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="electricite", type="integer")
     */
    private $electricite;

    /**
     * @var int
     *
     * @ORM\Column(name="eau", type="integer")
     */
    private $eau;

    /**
     * @var int
     *
     * @ORM\Column(name="decoration", type="integer")
     */
    private $decoration;

    /**
     * @var int
     *
     * @ORM\Column(name="salaires", type="integer")
     */
    private $salaires;

    /**
     * @var int
     *
     * @ORM\Column(name="autres", type="integer")
     */
    private $autres;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set electricite
     *
     * @param integer $electricite
     *
     * @return Charges
     */
    public function setElectricite($electricite)
    {
        $this->electricite = $electricite;

        return $this;
    }

    /**
     * Get electricite
     *
     * @return int
     */
    public function getElectricite()
    {
        return $this->electricite;
    }

    /**
     * Set eau
     *
     * @param integer $eau
     *
     * @return Charges
     */
    public function setEau($eau)
    {
        $this->eau = $eau;

        return $this;
    }

    /**
     * Get eau
     *
     * @return int
     */
    public function getEau()
    {
        return $this->eau;
    }

    /**
     * Set decoration
     *
     * @param integer $decoration
     *
     * @return Charges
     */
    public function setDecoration($decoration)
    {
        $this->decoration = $decoration;

        return $this;
    }

    /**
     * Get decoration
     *
     * @return int
     */
    public function getDecoration()
    {
        return $this->decoration;
    }

    /**
     * Set salaires
     *
     * @param integer $salaires
     *
     * @return Charges
     */
    public function setSalaires($salaires)
    {
        $this->salaires = $salaires;

        return $this;
    }

    /**
     * Get salaires
     *
     * @return int
     */
    public function getSalaires()
    {
        return $this->salaires;
    }

    /**
     * Set autres
     *
     * @param integer $autres
     *
     * @return Charges
     */
    public function setAutres($autres)
    {
        $this->autres = $autres;

        return $this;
    }

    /**
     * Get autres
     *
     * @return int
     */
    public function getAutres()
    {
        return $this->autres;
    }
}

