<?php

namespace CrecheBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classes
 *
 * @ORM\Table(name="classes")
 * @ORM\Entity(repositoryClass="CrecheBundle\Repository\ClassesRepository")
 */
class Classes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var Eleves
     *
     * @ORM\ManyToOne(targetEntity="Eleves", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $eleves;

    /**
     * @var string
     *
     * @ORM\Column(name="groupe", type="string", length=255)
     */
    private $groupe;

    /**
     * @var bool
     *
     * @ORM\Column(name="isPension", type="boolean")
     */
    private $isPension;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set groupe
     *
     * @param string $groupe
     *
     * @return Classes
     */
    public function setGroupe($groupe)
    {
        $this->groupe = $groupe;

        return $this;
    }

    /**
     * Get groupe
     *
     * @return string
     */
    public function getGroupe()
    {
        return $this->groupe;
    }

    /**
     * Set isPension
     *
     * @param boolean $isPension
     *
     * @return Classes
     */
    public function setIsPension($isPension)
    {
        $this->isPension = $isPension;

        return $this;
    }

    /**
     * Get isPension
     *
     * @return bool
     */
    public function getIsPension()
    {
        return $this->isPension;
    }

    /**
     * Set eleves
     *
     * @param \CrecheBundle\Entity\Eleves $eleves
     *
     * @return Classes
     */
    public function setEleves(\CrecheBundle\Entity\Eleves $eleves)
    {
        $this->eleves = $eleves;

        return $this;
    }

    /**
     * Get eleves
     *
     * @return \CrecheBundle\Entity\Eleves
     */
    public function getEleves()
    {
        return $this->eleves;
    }
}
