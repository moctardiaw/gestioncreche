<?php

namespace CrecheBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Paiements
 *
 * @ORM\Table(name="paiements")
 * @ORM\Entity(repositoryClass="CrecheBundle\Repository\PaiementsRepository")
 */
class Paiements
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
     /**
     * @var Mois
     *
     * @ORM\ManyToOne(targetEntity="Mois", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $mois;
   

    /**
     * @var string
     *
     * @ORM\Column(name="dateDePaiement", type="string", length=255)
     */
    private $dateDePaiement;

    /**
     * @var bool
     *
     * @ORM\Column(name="isPayed", type="boolean")
     */
    private $isPayed;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateDePaiement
     *
     * @param string $dateDePaiement
     *
     * @return Paiements
     */
    public function setDateDePaiement($dateDePaiement)
    {
        $this->dateDePaiement = $dateDePaiement;

        return $this;
    }

    /**
     * Get dateDePaiement
     *
     * @return string
     */
    public function getDateDePaiement()
    {
        return $this->dateDePaiement;
    }

    /**
     * Set isPayed
     *
     * @param boolean $isPayed
     *
     * @return Paiements
     */
    public function setIsPayed($isPayed)
    {
        $this->isPayed = $isPayed;

        return $this;
    }

    /**
     * Get isPayed
     *
     * @return bool
     */
    public function getIsPayed()
    {
        return $this->isPayed;
    }

    /**
     * Set mois
     *
     * @param \CrecheBundle\Entity\Mois $mois
     *
     * @return Paiements
     */
    public function setMois(\CrecheBundle\Entity\Mois $mois)
    {
        $this->mois = $mois;

        return $this;
    }

    /**
     * Get mois
     *
     * @return \CrecheBundle\Entity\Mois
     */
    public function getMois()
    {
        return $this->mois;
    }
}
