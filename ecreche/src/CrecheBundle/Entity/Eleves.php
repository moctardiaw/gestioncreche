<?php

namespace CrecheBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Eleves
 *
 * @ORM\Table(name="eleves")
 * @ORM\Entity(repositoryClass="CrecheBundle\Repository\ElevesRepository")
 */
class Eleves
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
     /**
     * @var Paiements
     *
     * @ORM\ManyToOne(targetEntity="Paiements", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $paiements;
    
    /**
     * @var Classes
     *
     * @ORM\ManyToOne(targetEntity="Classes", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $classes;

    /**
     * @var string
     *
     * @ORM\Column(name="nomEleve", type="string", length=255)
     */
    private $nomEleve;

    /**
     * @var string
     *
     * @ORM\Column(name="prenomEleve", type="string", length=255)
     */
    private $prenomEleve;

    /**
     * @var string
     *
     * @ORM\Column(name="dateDeNaissance", type="string", length=255)
     */
    private $dateDeNaissance;
    
    /**
     * @var string
     *
     * @ORM\Column(name="nomPere", type="string", length=255)
     */
    private $nomPere;
    
    /**
     * @var string
     *
     * @ORM\Column(name="prenomPere", type="string", length=255)
     */
    private $prenomPere;
    
    /**
     * @var string
     *
     * @ORM\Column(name="nomMere", type="string", length=255)
     */
    private $nomMere;
    
    /**
     * @var string
     *
     * @ORM\Column(name="prenomMere", type="string", length=255)
     */
    private $prenomMere;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomEleve
     *
     * @param string $nomEleve
     *
     * @return Eleves
     */
    public function setNomEleve($nomEleve)
    {
        $this->nomEleve = $nomEleve;

        return $this;
    }

    /**
     * Get nomEleve
     *
     * @return string
     */
    public function getNomEleve()
    {
        return $this->nomEleve;
    }

    /**
     * Set prenomEleve
     *
     * @param string $prenomEleve
     *
     * @return Eleves
     */
    public function setPrenomEleve($prenomEleve)
    {
        $this->prenomEleve = $prenomEleve;

        return $this;
    }

    /**
     * Get prenomEleve
     *
     * @return string
     */
    public function getPrenomEleve()
    {
        return $this->prenomEleve;
    }

    /**
     * Set dateDeNaissance
     *
     * @param string $dateDeNaissance
     *
     * @return Eleves
     */
    public function setDateDeNaissance($dateDeNaissance)
    {
        $this->dateDeNaissance = $dateDeNaissance;

        return $this;
    }

    /**
     * Get dateDeNaissance
     *
     * @return string
     */
    public function getDateDeNaissance()
    {
        return $this->dateDeNaissance;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->paiements = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add paiement
     *
     * @param \CrecheBundle\Entity\Paiements $paiement
     *
     * @return Eleves
     */
    public function addPaiement(\CrecheBundle\Entity\Paiements $paiement)
    {
        $this->paiements[] = $paiement;

        return $this;
    }

    /**
     * Remove paiement
     *
     * @param \CrecheBundle\Entity\Paiements $paiement
     */
    public function removePaiement(\CrecheBundle\Entity\Paiements $paiement)
    {
        $this->paiements->removeElement($paiement);
    }

    /**
     * Get paiements
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPaiements()
    {
        return $this->paiements;
    }

    /**
     * Set parents
     *
     * @param \CrecheBundle\Entity\Parents $parents
     *
     * @return Eleves
     */
    public function setParents(\CrecheBundle\Entity\Parents $parents = null)
    {
        $this->parents = $parents;

        return $this;
    }

    /**
     * Get parents
     *
     * @return \CrecheBundle\Entity\Parents
     */
    public function getParents()
    {
        return $this->parents;
    }

    /**
     * Set nomPere
     *
     * @param string $nomPere
     *
     * @return Eleves
     */
    public function setNomPere($nomPere)
    {
        $this->nomPere = $nomPere;

        return $this;
    }

    /**
     * Get nomPere
     *
     * @return string
     */
    public function getNomPere()
    {
        return $this->nomPere;
    }

    /**
     * Set prenomPere
     *
     * @param string $prenomPere
     *
     * @return Eleves
     */
    public function setPrenomPere($prenomPere)
    {
        $this->prenomPere = $prenomPere;

        return $this;
    }

    /**
     * Get prenomPere
     *
     * @return string
     */
    public function getPrenomPere()
    {
        return $this->prenomPere;
    }

    /**
     * Set nomMere
     *
     * @param string $nomMere
     *
     * @return Eleves
     */
    public function setNomMere($nomMere)
    {
        $this->nomMere = $nomMere;

        return $this;
    }

    /**
     * Get nomMere
     *
     * @return string
     */
    public function getNomMere()
    {
        return $this->nomMere;
    }

    /**
     * Set prenomMere
     *
     * @param string $prenomMere
     *
     * @return Eleves
     */
    public function setPrenomMere($prenomMere)
    {
        $this->prenomMere = $prenomMere;

        return $this;
    }

    /**
     * Get prenomMere
     *
     * @return string
     */
    public function getPrenomMere()
    {
        return $this->prenomMere;
    }

    /**
     * Set paiements
     *
     * @param \CrecheBundle\Entity\Paiements $paiements
     *
     * @return Eleves
     */
    public function setPaiements(\CrecheBundle\Entity\Paiements $paiements)
    {
        $this->paiements = $paiements;

        return $this;
    }

    /**
     * Set classes
     *
     * @param \CrecheBundle\Entity\Classes $classes
     *
     * @return Eleves
     */
    public function setClasses(\CrecheBundle\Entity\Classes $classes)
    {
        $this->classes = $classes;

        return $this;
    }

    /**
     * Get classes
     *
     * @return \CrecheBundle\Entity\Classes
     */
    public function getClasses()
    {
        return $this->classes;
    }
}
