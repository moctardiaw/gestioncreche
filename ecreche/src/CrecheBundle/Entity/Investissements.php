<?php

namespace CrecheBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Investissements
 *
 * @ORM\Table(name="investissements")
 * @ORM\Entity(repositoryClass="CrecheBundle\Repository\InvestissementsRepository")
 */
class Investissements
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="fournitures", type="integer")
     */
    private $fournitures;

    /**
     * @var int
     *
     * @ORM\Column(name="blouses", type="integer")
     */
    private $blouses;

    /**
     * @var int
     *
     * @ORM\Column(name="sorties", type="integer")
     */
    private $sorties;

    /**
     * @var int
     *
     * @ORM\Column(name="autres", type="integer")
     */
    private $autres;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fournitures
     *
     * @param integer $fournitures
     *
     * @return Investissements
     */
    public function setFournitures($fournitures)
    {
        $this->fournitures = $fournitures;

        return $this;
    }

    /**
     * Get fournitures
     *
     * @return int
     */
    public function getFournitures()
    {
        return $this->fournitures;
    }

    /**
     * Set blouses
     *
     * @param integer $blouses
     *
     * @return Investissements
     */
    public function setBlouses($blouses)
    {
        $this->blouses = $blouses;

        return $this;
    }

    /**
     * Get blouses
     *
     * @return int
     */
    public function getBlouses()
    {
        return $this->blouses;
    }

    /**
     * Set sorties
     *
     * @param integer $sorties
     *
     * @return Investissements
     */
    public function setSorties($sorties)
    {
        $this->sorties = $sorties;

        return $this;
    }

    /**
     * Get sorties
     *
     * @return int
     */
    public function getSorties()
    {
        return $this->sorties;
    }

    /**
     * Set autres
     *
     * @param integer $autres
     *
     * @return Investissements
     */
    public function setAutres($autres)
    {
        $this->autres = $autres;

        return $this;
    }

    /**
     * Get autres
     *
     * @return int
     */
    public function getAutres()
    {
        return $this->autres;
    }
}

