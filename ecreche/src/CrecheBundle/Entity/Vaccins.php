<?php

namespace CrecheBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vaccins
 *
 * @ORM\Table(name="vaccins")
 * @ORM\Entity(repositoryClass="CrecheBundle\Repository\VaccinsRepository")
 */
class Vaccins
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nomVaccin", type="string", length=255)
     */
    private $nomVaccin;

    /**
     * @var bool
     *
     * @ORM\Column(name="isCurrent", type="boolean")
     */
    private $isCurrent;

    /**
     * @var bool
     *
     * @ORM\Column(name="isRappel", type="boolean")
     */
    private $isRappel;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomVaccin
     *
     * @param string $nomVaccin
     *
     * @return Vaccins
     */
    public function setNomVaccin($nomVaccin)
    {
        $this->nomVaccin = $nomVaccin;

        return $this;
    }

    /**
     * Get nomVaccin
     *
     * @return string
     */
    public function getNomVaccin()
    {
        return $this->nomVaccin;
    }

    /**
     * Set isCurrent
     *
     * @param boolean $isCurrent
     *
     * @return Vaccins
     */
    public function setIsCurrent($isCurrent)
    {
        $this->isCurrent = $isCurrent;

        return $this;
    }

    /**
     * Get isCurrent
     *
     * @return bool
     */
    public function getIsCurrent()
    {
        return $this->isCurrent;
    }

    /**
     * Set isRappel
     *
     * @param boolean $isRappel
     *
     * @return Vaccins
     */
    public function setIsRappel($isRappel)
    {
        $this->isRappel = $isRappel;

        return $this;
    }

    /**
     * Get isRappel
     *
     * @return bool
     */
    public function getIsRappel()
    {
        return $this->isRappel;
    }
}

