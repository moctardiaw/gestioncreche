<?php

namespace CrecheBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ChargesController extends Controller
{
    /**
     * @Route("charges/create")
     */
    public function createAction()
    {
        return $this->render('CrecheBundle:Charges:create.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("charges/read")
     */
    public function readAction()
    {
        return $this->render('CrecheBundle:Charges:read.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("charges/update")
     */
    public function updateAction()
    {
        return $this->render('CrecheBundle:Charges:update.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("charges/delete")
     */
    public function deleteAction()
    {
        return $this->render('CrecheBundle:Charges:delete.html.twig', array(
            // ...
        ));
    }

}
