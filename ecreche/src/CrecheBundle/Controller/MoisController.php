<?php

namespace CrecheBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use CrecheBundle\Entity\Mois;


class MoisController extends Controller
{
    /**
     * @Route("mois/create")
     */
    public function createAction(Request $request)
    {
        $mois = new Mois();
       
        $form = $this->createForm(\CrecheBundle\Form\MoisType::class,$mois);       
        $form->handleRequest($request);
       
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($mois);
            $em->flush();
           
            $this->addFlash('noticeEleveAdd','Le nouvel élève '.$mois->getNomMois().' - '.$mois->getNomMois().' a été ajouté avec succès...');        
         return $this->redirectToRoute('inscription_creche');
        }      
           $formView = $form->createView();
        return $this->render('CrecheBundle:Mois:create.html.twig', array(
           'form'=>$formView
        ));
    }

    /**
     * @Route("mois/read")
     */
    public function readAction()
    {
        return $this->render('CrecheBundle:Mois:read.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("mois/update")
     */
    public function updateAction()
    {
        return $this->render('CrecheBundle:Mois:update.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("mois/delete")
     */
    public function deleteAction()
    {
        return $this->render('CrecheBundle:Mois:delete.html.twig', array(
            // ...
        ));
    }

}
