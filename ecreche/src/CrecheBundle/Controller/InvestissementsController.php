<?php

namespace CrecheBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class InvestissementsController extends Controller
{
    /**
     * @Route("investissements/create")
     */
    public function createAction()
    {
        return $this->render('CrecheBundle:Investissements:create.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("investissements/read")
     */
    public function readAction()
    {
        return $this->render('CrecheBundle:Investissements:read.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("investissements/update")
     */
    public function updateAction()
    {
        return $this->render('CrecheBundle:Investissements:update.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("investissements/delete")
     */
    public function deleteAction()
    {
        return $this->render('CrecheBundle:Investissements:delete.html.twig', array(
            // ...
        ));
    }

}
