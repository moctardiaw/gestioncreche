<?php

namespace CrecheBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ClassesController extends Controller
{
    /**
     * @Route("classes/create")
     */
    public function createAction()
    {
        return $this->render('CrecheBundle:Classes:create.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("classes/read")
     */
    public function readAction()
    {
        return $this->render('CrecheBundle:Classes:read.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("classes/update")
     */
    public function updateAction()
    {
        return $this->render('CrecheBundle:Classes:update.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("classes/delete")
     */
    public function deleteAction()
    {
        return $this->render('CrecheBundle:Classes:delete.html.twig', array(
            // ...
        ));
    }

}
