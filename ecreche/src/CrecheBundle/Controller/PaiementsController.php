<?php

namespace CrecheBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class PaiementsController extends Controller
{
    /**
     * @Route("paiements/create")
     */
    public function createAction()
    {
        return $this->render('CrecheBundle:Paiements:create.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("paiements/read")
     */
    public function readAction()
    {
        return $this->render('CrecheBundle:Paiements:read.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("paiements/update")
     */
    public function updateAction()
    {
        return $this->render('CrecheBundle:Paiements:update.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("paiements/delete")
     */
    public function deleteAction()
    {
        return $this->render('CrecheBundle:Paiements:delete.html.twig', array(
            // ...
        ));
    }

}
