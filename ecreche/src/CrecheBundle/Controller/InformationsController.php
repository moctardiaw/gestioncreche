<?php

namespace CrecheBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class InformationsController extends Controller
{
    /**
     * @Route("informations/create")
     */
    public function createAction()
    {
        return $this->render('CrecheBundle:Informations:create.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("informations/read")
     */
    public function readAction()
    {
        return $this->render('CrecheBundle:Informations:read.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("informations/update")
     */
    public function updateAction()
    {
        return $this->render('CrecheBundle:Informations:update.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("informations/delete")
     */
    public function deleteAction()
    {
        return $this->render('CrecheBundle:Informations:delete.html.twig', array(
            // ...
        ));
    }

}
