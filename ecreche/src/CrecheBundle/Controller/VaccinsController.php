<?php

namespace CrecheBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class VaccinsController extends Controller
{
    /**
     * @Route("vaccins/create")
     */
    public function createAction()
    {
        return $this->render('CrecheBundle:Vaccins:create.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("vaccins/read")
     */
    public function readAction()
    {
        return $this->render('CrecheBundle:Vaccins:read.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("vaccins/update")
     */
    public function updateAction()
    {
        return $this->render('CrecheBundle:Vaccins:update.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("vaccins/delete")
     */
    public function deleteAction()
    {
        return $this->render('CrecheBundle:Vaccins:delete.html.twig', array(
            // ...
        ));
    }

}
