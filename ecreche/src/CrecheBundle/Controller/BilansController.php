<?php

namespace CrecheBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class BilansController extends Controller
{
    /**
     * @Route("bilans/create")
     */
    public function createAction()
    {
        return $this->render('CrecheBundle:Bilans:create.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("bilans/read")
     */
    public function readAction()
    {
        return $this->render('CrecheBundle:Bilans:read.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("bilans/update")
     */
    public function updateAction()
    {
        return $this->render('CrecheBundle:Bilans:update.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("bilans/delete")
     */
    public function deleteAction()
    {
        return $this->render('CrecheBundle:Bilans:delete.html.twig', array(
            // ...
        ));
    }

}
