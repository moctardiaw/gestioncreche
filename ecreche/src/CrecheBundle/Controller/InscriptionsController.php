<?php

namespace CrecheBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use CrecheBundle\Entity\Eleves;


class InscriptionsController extends Controller
{
    /**
     * @Route("inscriptions/create")
     */
    public function createAction(Request $request)
    {
        $eleves = new Eleves();
       
        $form = $this->createForm(\CrecheBundle\Form\ElevesType::class,$eleves);       
        $form->handleRequest($request);
       
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($eleves);
            $em->flush();
           
            $this->addFlash('noticeEleveAdd','Le nouvel élève '.$eleves->getPrenom().' - '.$eleves->getNom().' a été ajouté avec succès...');        
         return $this->redirectToRoute('inscription_creche');
        }      
           $formView = $form->createView();
        return $this->render('CrecheBundle:Inscriptions:create.html.twig', array(
           'form'=>$formView
        ));
    }

    /**
     * @Route("inscriptions/read")
     */
    public function readAction()
    {
        return $this->render('CrecheBundle:Inscriptions:read.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("inscriptions/update")
     */
    public function updateAction()
    {
        return $this->render('CrecheBundle:Inscriptions:update.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("inscriptions/delete")
     */
    public function deleteAction()
    {
        return $this->render('CrecheBundle:Inscriptions:delete.html.twig', array(
            // ...
        ));
    }

}
